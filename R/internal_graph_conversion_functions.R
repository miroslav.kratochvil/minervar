### Utility functions for converting MINERVA diagrams to graphs and vice versa

###
### Bipartite transformations
###

### A helper function to create a bipartite version of a given interaction
create_bipartite_reaction <- function(id, reactants, products, modifiers, type) {
  interactions <- rbind(cbind(from = reactants$aliasId, to = id, role = "consumption"),
                        cbind(from = id, to = products$aliasId, role = "production"))
  elements <- rbind(cbind(name = id, role = "reaction", type = type),
                    cbind(name = reactants$aliasId, role = "element", type = ""),
                    cbind(name = products$aliasId, role = "element", type = ""))
  if(length(modifiers) > 0) {
    interactions <- rbind(interactions,
                          cbind(from = modifiers$aliasId, to = id,
                                role = modifiers$type))
    elements <- rbind(elements,
                      cbind(name = modifiers$aliasId, role = "element", type = ""))
  }
  return(list(interactions = interactions, elements = elements))
}


### A helper function to create a bipartite version of diagram reactions from their reactants, products and modifiers
create_bipartite_diagram <- function(diagram_reactions, diagram_elements) {
  ### Apply the 'create_bipartite_reaction' function to required fields
  bipartite_reactions <- dplyr::select(diagram_reactions,
                                       "id", "reactants", "products", "modifiers", "type") %>%
    purrr::pmap(create_bipartite_reaction) %>%
    purrr::transpose() ### Transpose to group interactions and elements

  ### Transform interactions and elements into a data.frame
  interactions <- data.frame(purrr::reduce(bipartite_reactions$interactions, rbind))
  elements <- unique(data.frame(purrr::reduce(bipartite_reactions$elements, rbind)))

  ### Add 'type' and 'name' to elements, use 'diagram_elements' data frame
  elements <- merge(elements, diagram_elements[, c("id", "type", "name")],
                    by.x = "name", by.y = "id", all.x = T) %>%
    dplyr::mutate(type = dplyr::case_when(role == "reaction" ~ .data[["type.x"]], TRUE ~ .data[["type.y"]])) %>%
    dplyr::select(-"type.x", -"type.y") %>%
    dplyr::rename("in_diag_name" = "name.y")

  return(list(interactions = data.frame(interactions), elements = elements))
}

###
### SIF to GPML transformations
###

### The igraph object should contain:
### label, type, color
### 'id' will be inferred from 'name'
extract_igraph_data <- function(gmpl_graph, expansion_coefficient = 100) {
  ### Read node information from the igraph object ("name", "label", "type", "color")
  nodedata <- data.frame(igraph::vertex.attributes(gmpl_graph)) %>%
    dplyr::select("name", "label", "type", "color") %>%
    dplyr::rename("id" = "name")
  ### Layout the graph and transform the results
  gl <- igraph::layout_with_fr(gmpl_graph, niter = 1000)
  ### Scale, the constant is based on the defaults height and width (see below)
  gl <- gl*expansion_coefficient/min(stats::dist(gl))
  ### Correct for negative values, add 100 margin
  gl[,1] <- gl[,1]+abs(min(gl[,1]))+100
  gl[,2] <- gl[,2]+abs(min(gl[,2]))+100
  ### Add positions and dimensions
  nodedata <- cbind(nodedata, x = gl[,1], y = gl[,2], w = 60, h = 30)

  ### Read node information from the igraph object ("color", "thickness")
  edgedata <- data.frame(igraph::edge.attributes(gmpl_graph)) %>%
    dplyr::select(dplyr::any_of(c("type", "color", "thickness", "evidence")))
  ### Add source and target information based on the igraph::get.edgelist
  el <- igraph::get.edgelist(gmpl_graph)
  edgedata <- cbind(source = el[,1], target = el[,2], edgedata)

  return(list(nodes = nodedata, edges = edgedata))
}

### Initialise a GPML::Pathway
create_gpml_pathway <- function(name, version = date(), organism = "Homo sapiens",
                                nodes, edges) {
  ### GPML namespaces to use
  ns_gpml <- xml2::xml_ns_rename(
    xml2::xml_ns(xml2::read_xml("<root>
                                    <gpml xmlns = 'http://pathvisio.org/GPML/2013a' />
                                  </root>")), d1 = "gpml")

  ### Create a GPML::DataNode object, with given label, id, type position and size
  create_DataNode <- function(id, label, type, x, y, w, h, color,
                              zorder = "32768", fontsize="12", valign="Middle") {
    return(xml2::read_xml(
      paste0("<DataNode TextLabel='", label, "' GraphId='", id, "' Type='", type, "'>",
             "<Graphics CenterX='", x, "' CenterY='", y, "' Width='", w,
             "' Height='", h,"' ZOrder='", zorder,"' FontSize='", fontsize,
             "' Valign='", valign, "' FillColor = '", color,"'/>",
             "<Xref Database='' ID='' />",
             "</DataNode>")))
  }

  ### Add a GPML::DataNode to a GPML::Pathway
  add_DataNode_to_Pathway <- function(gpml_pathway, gpml_node) {
    xml2::xml_add_child(gpml_pathway, gpml_node)
    canvas <- xml2::xml_find_first(gpml_pathway, "//gpml:Pathway/gpml:Graphics", ns_gpml)
    node_graphics <- xml2::xml_find_first(gpml_node, "./Graphics")
    ### Resize the canvas if needed
    xml2::xml_attr(canvas, "BoardWidth") <- max(as.numeric(xml2::xml_attr(canvas, "BoardWidth")),
                                                as.numeric(xml2::xml_attr(node_graphics, "CenterX")) +
                                                  as.numeric(xml2::xml_attr(node_graphics, "Width")))
    xml2::xml_attr(canvas, "BoardHeight") <- max(as.numeric(xml2::xml_attr(canvas, "BoardHeight")),
                                                 as.numeric(xml2::xml_attr(node_graphics, "CenterY")) +
                                                   as.numeric(xml2::xml_attr(node_graphics, "Height")))
  }

  ### Add a GPML::Interaction to a GPML::Pathway, based on the ids of two GPML::DataNodes
  add_Interaction_to_Pathway <- function(source, target, type, color = "ffffff", thickness = "1.0", evidence = "",
                                         gpml_pathway, biopax_refs = NULL) {
    dn_src <- xml2::xml_find_first(gpml_pathway, paste0("//DataNode[@GraphId='", source, "']"))
    dn_trg <- xml2::xml_find_first(gpml_pathway, paste0("//DataNode[@GraphId='", target, "']"))
    ### Identify anchor points
    gpml_ps <- xml2::read_xml(paste0("<Point X='", as.numeric(xml2::xml_attr(xml2::xml_child(dn_src),"CenterX")),# + as.numeric(xml_attr(xml_child(dn_src),"Width"))/2,
                                     "' Y='", xml2::xml_attr(xml2::xml_child(dn_src),"CenterY"),
                                     "' GraphRef='", xml2::xml_attr(dn_src,"GraphId"), "' RelX='1.0' RelY='0.0' />"))
    gpml_pt <- xml2::read_xml(paste0("<Point X='", as.numeric(xml2::xml_attr(xml2::xml_child(dn_trg),"CenterX")),# - as.numeric(xml_attr(xml_child(dn_trg),"Width"))/2,
                                     "' Y='", xml2::xml_attr(xml2::xml_child(dn_trg),"CenterY"),
                                     "' GraphRef='", xml2::xml_attr(dn_trg,"GraphId"), "' RelX='-1.0' RelY='0.0' ArrowHead='",type,"'/>"))

    gpml_gr <- xml2::read_xml(paste0("<Graphics ZOrder='12288' LineThickness='", thickness,"' Color='", color,"'/>"))

    xml2::xml_add_child(gpml_gr, gpml_ps)
    xml2::xml_add_child(gpml_gr, gpml_pt)

    gpml_ix <- xml2::read_xml("<Interaction/>")
    xml2::xml_add_child(gpml_ix, gpml_gr)
    xml2::xml_add_child(gpml_ix, xml2::read_xml("<Xref Database='' ID='' />"))

    ### Map back to Biopax references' identifers
    purrr::walk(strsplit(evidence, split = ";")[[1]],
                ~ xml2::xml_add_child(gpml_ix,
                                      xml2::read_xml(paste0("<BiopaxRef>",
                                                            with(biopax_refs, id[entry == .]),
                                                            "</BiopaxRef>"))))
    xml2::xml_add_child(gpml_pathway, gpml_ix)
  }

  xgmml_P <- xml2::read_xml(paste0("<Pathway xmlns='http://pathvisio.org/GPML/2013a' Name='", name,
                                   "' Version='", version,"' Organism='", organism, "'>",
                                   "<Graphics BoardWidth='0.0' BoardHeight='0.0' />",
                                   "</Pathway>"))

  purrr::pmap(nodes, create_DataNode) %>%
    purrr::walk(~ add_DataNode_to_Pathway(xgmml_P, .))

  ### If evidence is available, create a list for all unique items,
  ### with a mapping of evidence to annotation tag identifier
  biopax_entries <- edges$evidence
  xgmml_Bp <- xml2::read_xml("<Biopax/>")
  if(!is.null(biopax_entries)) {
    biopax_entries <- strsplit(biopax_entries, split = ";") %>%
      unlist() %>% unique()
    ### Create identifiers
    biopax_entries <- data.frame(id = paste0("ann",seq_along(biopax_entries)),
                                 entry = biopax_entries)
    ### A function translating a string 'database:identifier' into a BioPAX entry
    bp_entry_to_node <- function(id, entry) {
      parts <- strsplit(entry, split = ":")[[1]]
      paste0("<bp:PublicationXref xmlns:bp='http://www.biopax.org/release/biopax-level3.owl#' xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#' ",
             "rdf:id='",id,"'>",
             "<bp:ID rdf:datatype='http://www.w3.org/2001/XMLSchema#string'>",parts[2],"</bp:ID>",
             "<bp:DB rdf:datatype='http://www.w3.org/2001/XMLSchema#string'>",parts[1],"</bp:DB>",
             "</bp:PublicationXref>")
    }
    purrr::pmap(biopax_entries, bp_entry_to_node) %>%
      purrr::walk(~ xml2::xml_add_child(xgmml_Bp, xml2::read_xml(.)))
  }

  purrr::pwalk(edges, add_Interaction_to_Pathway,
               gpml_pathway = xgmml_P,
               biopax_refs = biopax_entries) %>%
    purrr::keep(~ length(.) > 0)

  if(!is.null(biopax_entries)) {
    xml2::xml_add_child(xgmml_P, xgmml_Bp)
  }

  return(xgmml_P)
}
