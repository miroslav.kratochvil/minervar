# minervaR

An R package for an interface to MINERVA API and helper functions.

This package supports interactions with disease and pathway maps hosted on 
[the MINERVA Platform](https://minerva.uni.lu). Functions implemented so far make it easier
to access maps, their structure and annotations, including projects shared via the MINERVA Net repository.

Another set of functions facilitates cross-format conversions, diagram merging, 
and supports basic manipulations of CellDesigner files.

Applications of the `minervaR` package are showcased in the following publications:
- ['Visualization of automatically combined disease maps and pathway diagrams for rare diseases' Gawron et al 2023](https://www.frontiersin.org/articles/10.3389/fbinf.2023.1101505/full)
- ['Exploration and comparison of molecular mechanisms across diseases using MINERVA Net' Gawron et al 2023](https://onlinelibrary.wiley.com/doi/10.1002/pro.4565)
