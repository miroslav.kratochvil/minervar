% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/get_bioentity_pathway_groupings.R
\name{get_bioentity_pathway_groupings}
\alias{get_bioentity_pathway_groupings}
\title{Create element-pathway groups from a \code{map_components} list}
\usage{
get_bioentity_pathway_groupings(map_components, with_entire_diagrams = FALSE)
}
\arguments{
\item{map_components}{(\code{list}) a list returned by the \code{minervar::get_map_components} function}

\item{with_entire_diagrams}{(\code{logical}) if TRUE then use the contents of the diagrams for grouping as well}
}
\value{
(\code{list}) a list of data frames with annotations for all components having this given type
}
\description{
Function retrieving element aliases associated with pathways of all models in the project.
Caution! Pathways are treated by name, multiple pathway entries with the same name will be pooled together.
Results correspond to the list of diagrams in a map.
}
\examples{

get_bioentity_pathway_groupings(example_map_components, TRUE)

}
