## example_network.R
### Get an example network (interactions)
example_network <- read.table("./inst/extdata/simple.sif", sep = "\t", header = T)

### Get an example network (interactions) with references
example_network_refs <- read.table("./inst/extdata/simple_ref.sif", sep = "\t", header = T)

### This network was created using the 'OmniPathR' package, not included here
### it was omitted not to include a dependency just for the sake of an example.
# example_network <- dplyr::filter(OmnipathR::import_all_interactions(),
#                                  is_directed == 1 & consensus_direction == 1 & !is.na(references)) %>%
#   dplyr::filter(source_genesymbol %in% c("SNCA", "PRKN", "PINK1")) %>%
#   dplyr::select(source_genesymbol, target_genesymbol)

usethis::use_data(example_network, overwrite = TRUE)
usethis::use_data(example_network_refs, overwrite = TRUE)
